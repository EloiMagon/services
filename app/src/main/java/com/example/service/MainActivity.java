package com.example.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
        public boolean started  = false;


        @Override
        protected void onCreate(Bundle savedInstanceState){
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
        }
        public void start(View view){
                Intent intent = new Intent(this,Horloge.class);
                started = (startService(intent)) != null;

        }
        public void stop(View view){
                Intent intent = new Intent(this,Horloge.class);
                started = !stopService(intent);


        }
        public void getCount(View view) {
                TextView tv = findViewById(R.id.textView);
                tv.setText("" + Horloge.getCount());
        }
}

