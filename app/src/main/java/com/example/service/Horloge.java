package com.example.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.concurrent.atomic.AtomicLong;

public class Horloge extends Service {
    private static Thread ticker;
    private static AtomicLong count;
    public Horloge(){
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        if (ticker == null){
            count = new AtomicLong();
            ticker = new Thread(new Ticker());
            ticker.start();
        }
        return super.onStartCommand(intent,flags, startId);
    }

    public static long getCount() {
        if (count != null) return (long) count.get();
        else return -1;
    }

    @Override
    public IBinder onBind(Intent intent){
        // TODO return the communication channel to the service
        throw new UnsupportedOperationException("Not yet implemented");

    }
    private class Ticker implements Runnable{
        @Override
        public void run(){
            try {
                Thread.sleep(1000);
                count.set(count.get() +1L);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
